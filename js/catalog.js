$(document).ready(function() {
	var catalogTable = $('#catalog-table').DataTable({
		"paging":   false,
        "info":     false,
        "language":{
        	"search":"",
        	"searchPlaceholder":"Поиск...",
        	"zeroRecords":"Товров по вашему запросу не найдено",
        },
        "oSearch": {"sSearch": window.catalogSearch}
	});
});