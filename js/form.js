$(document).ready(function() {
  conctactForm = new Form('.contacts__form','/formApi/send');
  window.popupOrderForm = new Form('.order-popup','/formApi/send');
});

function Form(_formSelector,_apiUrl) {
  var formEl;
  var inputNameEl,inputEmailEl,inputPhoneEl,productNameEl;
  var wrapNameEl,wrapEmailEl,wrapPhoneEl;
  var sendBtnEl;

  var validateStatus;

  this.send = send;
  this.clear = clear;

  init();

  function init() {
    formEl = $(_formSelector);
    productNameEl = $(_formSelector + ' .productName');
    inputNameEl = $(_formSelector + ' .formWrapName .formInput');
    inputEmailEl = $(_formSelector + ' .formWrapEmail .formInput');
    inputPhoneEl = $(_formSelector + ' .formWrapPhone .formInput');
    wrapNameEl = $(_formSelector + ' .formWrapName');
    wrapEmailEl = $(_formSelector + ' .formWrapEmail');
    wrapPhoneEl = $(_formSelector + ' .formWrapPhone');
    sendBtnEl = $(_formSelector + ' .formSendBtn');
    sendBtnEl.on("click",validate);
    inputPhoneEl.inputmask({"mask": "+7 (999) 999-9999"});
  }

  function validate() {
    validateStatus = {
      name:true,
      email:true,
      phone:true,
    }

    values = {
      name:inputNameEl.val(),
      email:inputEmailEl.val(),
      phone:inputPhoneEl.val(),
    }
    
    var validateTotal = true;
    if (values.name=="") {
      validateStatus.name =  false;
      validateTotal = false;
    }
    if (values.email=="") {
      validateStatus.email =  false;
      validateTotal = false;
    }
    
    if (values.phone=="" || !inputPhoneEl.inputmask("isComplete")) {
      validateStatus.phone =  false;
      validateTotal = false;
    }

    updateValidate(validateStatus);

    if (validateTotal) {
      formEl.addClass('sending');
      if (productNameEl.length!=0) {
        values.productName = productNameEl.text();
      }
      send(values);
    }   
  } 

  function updateValidate(status) {
    if (!status.name) {
      wrapNameEl.addClass('error');
    }
    else {
      wrapNameEl.removeClass('error');
    }
    if (!status.email) {
      wrapEmailEl.addClass('error');
    }
    else {
      wrapEmailEl.removeClass('error');
    }
    if (!status.phone) {
      wrapPhoneEl.addClass('error');
    }
    else {
      wrapPhoneEl.removeClass('error');
    }
  }

  function send(values) {    
    var data = generateData(values);
    values.date = data;
    $.ajax({
      type: "POST",
      url: _apiUrl,
      data: values,
      success: sendCallback,
      dataType: "json"
    });
  }

  function sendCallback(data) {
    formEl.removeClass('sending');
    if (data.status) {
      formEl.addClass('success');
    }
    else {
      formEl.addClass('fail');
    }
  }

  function generateData(values) {
    var result =  17*(1373 + values.name.length + values.email.length + values.phone.length);
    return result;
  }

  function clear() {
    inputNameEl.val('');
    inputEmailEl.val('');
    inputPhoneEl.val('');
    validateStatus = {
      name:true,
      email:true,
      phone:true,
    }
    updateValidate(validateStatus);
    formEl.removeClass('sending');
    formEl.removeClass('success');
    formEl.removeClass('fail');
  }

}