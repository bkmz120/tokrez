$(document).ready(function() {
	window.mMenu = new MMenu;
});

function MMenu() {
	var menu = document.querySelector('.mobile-menu');
	var menuBtn = document.querySelector('.mobile-menu-btn');
	var content = document.querySelector('.sitewrap');
	var contentInner = document.querySelector('.sitewrap__inner');
	var MENU_WIDTH = 240;

	this.show = show;
	this.close = close;

	function show(){
		//var winWidth = window.innerWidth || document.body.clientWidth;
		//var contentWidth = winWidth - MENU_WIDTH;
		//content.style.width = contentWidth + 'px';
		//contentInner.style.width = winWidth + 'px';

		menuBtn.className += " mobile-menu-btn_open";
		menu.className += " mobile-menu_visible";
		content.className += " sitewrap_menu-open";
		content.addEventListener('click',close);		
	}

	function close(){
		//content.style.width = "100%";
		//contentInner.style.width = "auto";
		content.removeEventListener('click',close);
		content.className = content.className.replace(new RegExp('(?:^|\\s)'+ 'sitewrap_menu-open' + '(?:\\s|$)'), ' ');
		menuBtn.className = menuBtn.className.replace(new RegExp('(?:^|\\s)'+ 'mobile-menu-btn_open' + '(?:\\s|$)'), ' ');
		menu.className = menu.className.replace(new RegExp('(?:^|\\s)'+ 'mobile-menu_visible' + '(?:\\s|$)'), ' ');
		
	}
}