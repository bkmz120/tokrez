<?php

class FormApiController extends CController
{
	public function actionSend() {
		$mailer = new Mailer;
		$response = new AjaxResponse;
		try {
			$mailer->sendContacts($_POST);
		}
		catch (Exception $e) {
			$response->setError($e->getMessage());
		}
		$response->send();
	}
}