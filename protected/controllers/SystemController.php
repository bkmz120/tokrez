<?php

class SystemController extends CController
{
	public function actionImportCutters(){
		$import = new Import;
		try {
			$import->cutters('cutters.csv');
		}
		catch (ImportException $e) {
			echo "Error in line #".$e->errorRowNum." Message:".$e->getMessage();
		}
	}

	public function actionImportPlates(){
		$import = new Import;
		try {
			$import->plates('plates.csv');
		}
		catch (ImportException $e) {
			echo "Error in line #".$e->errorRowNum." Message:".$e->getMessage();
		}
	}
}