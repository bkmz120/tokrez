<?php

class MainController extends CController
{
	public $layout='/layouts/layout';
	public $defaultAction = 'index';
	public $activeMenuLink = "index";

	public function actionIndex() {
		$catalogManager = new CatalogManager;
		$cutters = $catalogManager->getCutters(3);
		$plates = $catalogManager->getPlates(3);
		$this->activeMenuLink = "index";
		$this->render('index',array('cutters'=>$cutters,'plates'=>$plates));
	}

	public function actionCatalog($type='tokarnie-rezci',$search='') {	
		$catalogManager = new CatalogManager;
		$typeNotFound = false;

		switch ($type) {
			case 'tokarnie-rezci':
				$products = $catalogManager->getCutters();
				$catalogTemplate = "catalogCutters";
				$catalogTitle = "Токарные резцы";
				break;
			case 'tokarnie-plastini':
				$products = $catalogManager->getPlates();
				$catalogTemplate = "catalogPlates";
				$catalogTitle = "Токарные пластины";
				break;
			default:
				$typeNotFound = true;
		}
		if (!$typeNotFound) {
			$this->activeMenuLink = "catalog";
			$this->render($catalogTemplate,array('products'=>$products,
										  'catalogTitle'=>$catalogTitle,
										  'search'=>$search,
										));
		}		
	}

	public function actionContacts() {
		$this->activeMenuLink = "contacts";
		$this->render('contacts');
	}


	public function actionError() {
		if($error=Yii::app()->errorHandler->error)
		{
			$this->render('error', $error);
		}
	}
}
