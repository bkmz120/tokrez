<?php foreach ($products as $i=>$product): ?>
	<?php if ($i==0) $itemClass = "maincat__item_first"; 
		  elseif ($i==count($products)-1) $itemClass = "maincat__item_last";
		  else  $itemClass = "";
	?>
	<div class="maincat__item <?php echo $itemClass ?>">
		<div class="maincat__item-clm1">
			<div class="maincat__item-img-wr">
				<a href="<?php echo $product->photo1Url ?>" data-lightbox="img<?php echo $product->id ?>" data-title="<?php echo $product->name ?>">
					<img class="maincat__item-img" src="<?php echo $product->photo1ThmbUrl ?>">
				</a>
			</div>
			<?php if ($product->stock==1): ?>
				<div class="maincat__item-status">есть в наличии</div>
			<?php endif; ?>
		</div>
		<div class="maincat__item-clm2">
			<a class="maincat__item-title"  href="<?php echo $product->photo1Url ?>" data-lightbox="name<?php echo $product->id ?>" data-title="<?php echo $product->name ?>">
				<?php echo $product->name ?>
			</a>
			<div class="maincat__item-params">
				<div class="maincat__item-param">
					<div class="maincat__item-param-name">&nbsp;</div>
					<div class="maincat__item-param-val"><?php echo $product->comment ?></div>
				</div>
				<div class="maincat__item-param">
					<div class="maincat__item-param-name">Размер</div>
					<div class="maincat__item-param-val"><?php echo $product->size ?></div>
				</div>
				<div class="maincat__item-param">
					<div class="maincat__item-param-name">Производитель</div>
					<div class="maincat__item-param-val"><?php echo $product->country ?></div>
				</div>
			</div>
			<div class="maincat__item-bottom">
				<div class="maincat__item-price"><?php echo $product->price ?> р.</div>
				<div class="maincat__item-orderBtn" onclick="window.orderPopup.show('<?php echo $product->name ?>','<?php echo $product->photo1ThmbUrl ?>')">заказать</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>