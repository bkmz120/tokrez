<div class="contacts">
	<div class="contacts__info">
		<div class="contacts__info-line contacts__info-line_address">г.Челябинск, ул. 40 лет Октября, 33</div>
		<div class="contacts__info-line contacts__info-line_phone">8-950-749-34-64</div>
		<div class="contacts__info-line contacts__info-line_mail">info@tokrez.ru</div>
		<div class="contacts__info-line contacts__info-line_deliver">Доставка осуществляется во все регионы России</div>
	</div>
	<div class="contacts__form">
		<div class="contacts__form-head">ОБРАТНАЯ СВЯЗЬ</div>
		<div class="contacts__form-inputs">
			<div class="contacts__form-inp formWrapName">
				<div class="contacts__form-inp-label">Имя <span class="contacts__form-inp-req formError">обязательное поле</span></div>
				<input type="text" class="contacts__form-inp-val formInput">
			</div>
			<div class="contacts__form-inp formWrapPhone">
				<div class="contacts__form-inp-label">Телефон <span class="contacts__form-inp-req formError">обязательное поле</span></div>
				<input type="text" class="contacts__form-inp-val formInput">
			</div>
			<div class="contacts__form-inp formWrapEmail">
				<div class="contacts__form-inp-label">Email <span class="contacts__form-inp-req formError">обязательное поле</span></div>
				<input type="text" class="contacts__form-inp-val formInput">
			</div>
			<div class="contacts__form-inp">
				<div class="contacts__form-sendBtn formSendBtn">Отправить</div>
			</div>
		</div>
		<div class="contacts__form-sending">
			<img src="/img/loading.gif" class="contacts__form-sending-img">
		</div>
		<div class="contacts__form-success">
			<div class="contacts__form-success-text">Сообщение отправлено!</div>
		</div>
		<div class="contacts__form-fail">
			<div class="contacts__form-fail-text">Произошла ошибка. Сообщение не доставлено.</div>
		</div>
	</div>

</div> 