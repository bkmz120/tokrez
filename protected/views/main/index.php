<div class="main-slider">
	<div class="main-slider__slide main-slider__slide_plates">
		<div class="main-slider__slide-inner">
			<div class="main-slider__slide-info">
				Токарные пластины для обработки стали, аллюминия, чугуна, нержавеющей стали
			</div>
		</div>		
	</div>
	<div class="main-slider__slide main-slider__slide_cutters">
		<div class="main-slider__slide-inner">
			<div class="main-slider__slide-info">
				Токарные резцы от 10 до 32 габарита. Для обработки внутренних, наружных поверхностей, нерезания резьбы, отрезные
			</div>
		</div>		
	</div>	
</div>


<section class="maincat">
	<div class="maincat__head">
		<h1 class="maincat__title">Токарные резцы</h1>
		<form class="search-form" action="/catalog/tokarnie-rezci" >
			<input type="text" name="search" placeholder="Поиск по каталогу резцов" class="search-form__inp">
			<input type="submit" value="" class="search-form__btn">
		</form>
	</div>
	

	<div class="maincat__items">
		<?php $this->renderPartial('index/cuttersList',array('products'=>$cutters)) ?>	
		<?php $url ='/catalog/tokarnie-rezci' ?>
		<a class="maincat__allcats" href="<?php echo $url?>">Cмотреть весь каталог резцов</a>
	</div>
</section>

<section class="maincat maincat_last">
	<div class="maincat__head">
		<h1 class="maincat__title">Токарные пластины</h1>
		<form class="search-form" action="/catalog/tokarnie-plastini" >
			<input type="text" name="search" placeholder="Поиск по каталогу пластин" class="search-form__inp">
			<input type="submit" value="" class="search-form__btn">
		</form>
	</div>
	<div class="maincat__items">
		<?php $this->renderPartial('index/platesList',array('products'=>$plates)) ?>	
		<?php $url ='/catalog/tokarnie-plastini' ?>
		<a class="maincat__allcats" href="<?php echo $url?>">Cмотреть весь каталог пластин</a>
	</div>
</section>