'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var streamqueue  = require('streamqueue');
var autoprefixer = require('gulp-autoprefixer');
var uglifycss = require('gulp-uglifycss');
var minify = require('gulp-minify');
 
gulp.task('sass', function () {
  gulp.src('./css/scss/index.scss')
    .pipe(sass({outputStyle:"expanded"}).on('error', sass.logError))
    .pipe(concat('style.css'))
    .pipe(autoprefixer({
	    browsers: ['last 2 versions'],
	    cascade: false
	}))    
    .pipe(gulp.dest('./css'));
});


gulp.task('js-libs',function() {
  return streamqueue({ objectMode: true },
  		gulp.src('./js/libs/jquery.js'),
  		gulp.src('./js/libs/jquery.dataTables.min.js'),
  		gulp.src('./js/libs/lightbox/js/lightbox.min.js'),
  		gulp.src('./js/libs/slick/slick.min.js'),
      gulp.src('./js/libs/jquery.inputmask.bundle.min.js')
    )
  	.pipe(concat('libs.js'))
    .pipe(gulp.dest('./js/dist'));
});

gulp.task('js-source',function() {
  return streamqueue({ objectMode: true },
  		gulp.src('./js/mobilemenu.js'),
  		gulp.src('./js/form.js'),
  		gulp.src('./js/slider.js'),
  		gulp.src('./js/catalog.js'),
  		gulp.src('./js/orderpopup.js')
    )
  	.pipe(concat('build.js'))
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: [],
        ignoreFiles: []
    }))
    .pipe(gulp.dest('./js/dist'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./css/scss/**/*.scss', ['sass']);
});

gulp.task('default',function(){
  gulp.watch('./css/scss/**/*.scss', ['sass']);
  gulp.watch('./js/*.js', ['js-source']);
  gulp.watch('./js/libs/**/*.js', ['js-libs']);  
});


gulp.task('build', ['js-libs','js-source', 'sass']);